//Nombre: Fibonacci
//andres Renteria
//22/08/2016
//Se hace la funcion fibonacci (f(n-1)+f(n-2)) de manera recursiva
#include <stdio.h>

unsigned long long int fibonacci(unsigned long long int n,
                                 unsigned long long int *memoization){

  if(memoization != 0 || n == 0){

    return memoization[n];
    
  }

  memoization[n] = fibonacci(n-1, memoization) + fibonacci(n-2, memoization);

  return memoization[n];

}

int main(int argc, char **argv){

  unsigned long long int memoization[100]= {0,1,0};

  printf("el resultado del fibonacci_memo: %d, es: %llu \n",atoi(argv[1]), fibonacci( atoi(argv[1]), memoization));

  return 0;

}
