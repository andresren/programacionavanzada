//Nombre: Fibonacci
//andres Renteria
//22/08/2016
//Se hace la funcion fibonacci (f(n-1)+f(n-2)) de manera recursiva
#include <stdio.h>

unsigned long long int fibonacci(unsigned long long int n){
  if(n==0 || n==1){
    return n;
  }
  return fibonacci(n-1)+fibonacci(n-2);
}

int main(int argc, char **argv){

  printf("el resultado del fibonacci: %d, es: %llu \n",atoi(argv[1]), fibonacci( atoi(argv[1]) ));

  return 0;
}
