//Nombre: Kangarootest
//andres Renteria
//04/09/2016

#include <stdio.h>
#include "utest.h"
#include "string.h"
#include "../src/kangaroo.h"

static char * test_jump(){
  int a,b,c;
  a = 3;
  b = 5;
  c = 9;
  assert_test("the kanagroos must jump ", calculate_jump(a,b,c) == 3);
  return 0;

}

int main(){
  run_test("Test jump:",test_jump);
  return 0;

}
