//Nombre: Kangaroo
//andres Renteria
//04/09/2016
#include <stdio.h>
#include "string.h"
//calculate the max number of jumps
int calculate_jump(int a, int b, int c){
  //check if the conditions for a, b and c are met
  if(a >= b || a >= c || b >= c){
    printf("cant do jumps\n");
    return 0;
  }else if(a<=0 || c>=100){
    printf("check size of numbers\n");
    return 0;
  }
  //use de greater distance between b a and c b as this helps find where the max number of jumps will be at 
  if(b-a > c-b){
    return b-a-1;
  }else{
    return c-b-1;
  }

}
