//Nombre:farthest test
//andres Renteria
//28/11/2016

#include <stdio.h>
#include "utest.h"
#include "vector.h"
#include "../src/farthest.h"
#include "string.h"
#include <stdlib.h>
#include <assert.h>
#include <math.h>

static char * test_get_distance(){
  int x,y,a,b;
  double distance;
  x=0;
  a=0;
  y=0;
  b=5;
  distance = getDistance(x,y,a,b);
  assert_test("the offset must be 3", distance == 5);
  return 0;
}

int main(){
  run_test("Test get distance:",test_get_distance);

  return 0;

}
