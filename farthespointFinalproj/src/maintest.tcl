#!/usr/bin/expect -f
# For colors
proc capability cap {expr {![catch {exec tput -S << $cap}]}}
proc colorterm {} {expr {[capability setaf] && [capability setab]}}
proc tput args {exec tput -S << $args >/dev/tty}
array set color {black 0 red 1 green 2 yellow 3 blue 4 magenta 5 cyan 6 white 7}
proc foreground x {exec tput -S << "setaf $::color($x)" > /dev/tty}
proc background x {exec tput -S << "setab $::color($x)" > /dev/tty}
proc reset {} {exec tput sgr0 > /dev/tty}

#Test the program
eval spawn [lrange $argv 0 end]

#Put your test case here

send "3\r"
send "1 0\r"
send "0 0\r"
send "0 10\r"

send "7\r"
send "3 572\r"
send "51 301\r"
send "212 79\r"
send "921 196\r"
send "974 711\r"
send "793 948\r"
send "174 928\r"

send "0\r"
expect "3" {foreground green; puts "PASSED";reset} default {foreground red;puts "FAILED";reset}
expect "3" {foreground green; puts "PASSED";reset} default {foreground red;puts "FAILED";reset}
expect "1" {foreground green; puts "PASSED";reset} default {foreground red;puts "FAILED";reset}

expect "0" {foreground green; puts "PASSED";reset} default {foreground red;puts "FAILED";reset}
expect "4" {foreground green; puts "PASSED";reset} default {foreground red;puts "FAILED";reset}
expect "5" {foreground green; puts "PASSED";reset} default {foreground red;puts "FAILED";reset}
expect "6" {foreground green; puts "PASSED";reset} default {foreground red;puts "FAILED";reset}
expect "7" {foreground green; puts "PASSED";reset} default {foreground red;puts "FAILED";reset}
expect "2" {foreground green; puts "PASSED";reset} default {foreground red;puts "FAILED";reset}
expect "3" {foreground green; puts "PASSED";reset} default {foreground red;puts "FAILED";reset}
