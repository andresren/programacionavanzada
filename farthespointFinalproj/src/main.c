//Nombre:farthest test
//andres Renteria
//28/11/2016


#include <stdio.h>
#include <stdlib.h>
#include "farthest.c"
#include "vector.h"


int main(){
    int n,x;
    scanf("%d",&n);
    Vector result;
    int elemSize = sizeof(int);
    int initialAllocation = 1;
    VectorNew(&result,elemSize,initialAllocation);

    while(n!=0){
      int *a = malloc(sizeof(int) * n*2);
      double dist;
      int longest;
      int i,j;
      for(i = 0; i < n*2; i++){
         scanf("%d",&a[i]);
      }
      for(i=0; i < n*2; i=i+2){
        dist =0;
        for(j=0;j<n*2;j=j+2){

          if(i!=j){
            if(dist < getDistance(a[i],a[i+1],a[j],a[j+1])){
              dist = getDistance(a[i],a[i+1],a[j],a[j+1]);
              longest =j/2+1;
            }
          }
        }
        VectorAppend(&result,&longest);
        //printf("%d\n", longest);
      }
      scanf("%d",&n);
    }
    printOutput(result);

    return 0;
}
