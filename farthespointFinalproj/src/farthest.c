//Nombre:farthest test
//andres Renteria
//28/11/2016


#include <math.h>
#include "vector.h"
#include <stdio.h>

double getDistance(int a, int b, int x, int y){
  return sqrt((x-a)*(x-a)+(y-b)*(y-b));
}
void printOutput(Vector result){
  int k,j;
  for(j = 0;j < VectorLength(&result);j++){
    VectorNth(&result, &k, j);

    printf("%d\n", k);
  }
}
