#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>

int getnumber(int stairs,int *memo){    
    if(stairs == 0){
        return 1;
    }else if(stairs<0 ){
        return 0;
    }else if (memo[stairs] != 0){
        return memo[stairs];
    }else{
        memo[stairs] = getnumber(stairs-1,memo)+getnumber(stairs-2,memo) +getnumber(stairs-3,memo);
        return memo[stairs];
    }
}

int main(){
    int s;
    scanf("%d",&s);
    for(int a0 = 0; a0 < s; a0++){
        int n;
        scanf("%d",&n);
        int* memo = malloc(sizeof(int)*36);
        int result = getnumber(n,memo);
        printf("%d\n",result);
    }
    return 0;
}





