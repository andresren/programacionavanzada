#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>

int main(){
    int n;
    int k;
    int a_i;
    int point;
    scanf("%d %d",&n,&k);
    int *a = malloc(sizeof(int) * n);
    int *tmp = malloc(sizeof(int) * n);
    for(a_i = 0; a_i < n; a_i++){
       scanf("%d",&a[a_i]);
    }
    point = 0 - k;

    while(point < 0){
        point = point + n;

    }
    for(a_i = 0; a_i < n; a_i++){
       if(point >= n){
           point = point - n;
       }

       tmp[point] = a[a_i];
       point++;
    }
    for(a_i = 0; a_i < n; a_i++){
        printf("%d ", tmp[a_i]);
    }

    return 0;
}
