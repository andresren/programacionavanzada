//Nombre:Operations test
//andres Renteria
//29/08/2016
//pruebas para Operations
#include <stdio.h>
#include "utest.h"
#include "../src/operations.h"
#include "string.h"

static char * test_sum(){
  float a,b,c;
  a = 23.5;
  b = 5.0;
  c = sum(a,b);
  assert_test("Sum must be ", c == a + b);
  return 0;

}
static char * test_div(){
  float a,b,c;
  a = 23.5;
  b = 5.0;
  c = div(a,b);
  assert_test("Sum must be ", c == a / b);
  return 0;

}
static char * test_subtract(){
  float a,b,c;
  a = 23.5;
  b = 5.0;
  c = subtract(a,b);
  assert_test("Subtract must be ", c == 1);
  return 0;

}
static char * test_string2lower(){
  char *str_test = strdup("STRING in UPPerCase");
  char *str_expected = "string in uppercase";
  char *str_result = to_lower_string(str_test);
  assert_test("String must be on lower case ", !strcmp(str_expected, str_result));
  return 0;

}


int main(){
  run_test("Test sum:",test_sum);
  run_test("Test Subtract:",test_subtract);
  run_test("Test to lower", test_string2lower);
  return 0;

}
