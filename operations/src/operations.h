//Nombre:Operations header
//andres Renteria
//29/08/2016


#ifndef __OPERATIONSH__
#define __OPERATIONSH__

float sum(float a, float b);

float subtract(float a, float b);

float div(float a, float b);

char * to_lower_string(char *s);

#endif
