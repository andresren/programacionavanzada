//Nombre:Operations
//andres Renteria
//29/08/2016
//funciones para Operations
#include "operations.h"
#include <string.h>
#include <stdio.h>
float sum(float a, float b){
  return a+b;
}

float subtract(float a, float b){
  return a-b;
}

float div(float a, float b){
  return a/b;
}

char * to_lower_string(char *s){
  int i;
  for(i = 0; i < strlen(s);i++){
    s[i] = tolower(s[i]);
  }
  return s;
}
