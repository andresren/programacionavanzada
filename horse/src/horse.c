//Nombre:Horse
//andres Renteria
//20/10/2016

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include "vector.h"

int cmpInt(const void *elem1,const void *elem2)
{
  return (*(const int *)elem1-*(const int *)elem2);
}

int main(){
  int n,m,x,k,l;
  int i,j;
  int cmp = 1000;
  Vector result;
  int elemSize = sizeof(int);
  int initialAllocation = 1;


  scanf("%d",&n);

  for(i = 0; i < n; i++){
    VectorNew(&result,elemSize,initialAllocation);
    scanf("%d",&m);
    for (j = 0; j < m; j++) {
      scanf("%d",&x);
      VectorAppend(&result,&x);
    }
    VectorSort(&result, cmpInt);
    for(j=1;j<m;j++){
      VectorNth(&result, &k, j);
      VectorNth(&result, &l, j-1);
      if(k-l < cmp){
        cmp = k-l;
      }
    }
    printf("%d\n", cmp);
  }

  return 0;
}
