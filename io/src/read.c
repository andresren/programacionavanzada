
#include <stdio.h>
#include <stdlib.h>
unsigned debug;
#define printdebug(msg) if (debug) printf("%s\n",(msg))

int main(){
  debug = 0;
  char *value;
  char message[100];
  FILE *file;
  int num;
  long whence;
  value = getenv("DEBUG");
  if(value && strcmp(value,"1") == 0)
    debug = 1;

  file = fopen("input.txt","r");

  if(file == NULL){
    sprintf(message, "Error opening file ...");
    printdebug(message);
    return -1;
  }

  while(1){
    fscanf(file, "%d", &num);
    if(feof(file)) break;
    printf("%d\n",num * num);
  }
  fclose(file);
  return 0;

}
