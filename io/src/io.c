
#include <stdio.h>
#include <stdlib.h>
unsigned debug;
#define printdebug(msg) if (debug) printf("%s\n",(msg))

int main(){
  debug = 0;
  char *value;
  char message[100];
  FILE *file;
  int c;
  long whence;
  value = getenv("DEBUG");
  if(value && strcmp(value,"1") == 0)
    debug = 1;

  file = fopen("io.c","r");

  if(file == NULL){
    sprintf(message, "Error opening file ...");
    printdebug(message);
    return -1;
  }
  whence = ftell(file);
  while(1){
    c = fgetc(file);
    if(c == EOF){
      whence = ftell(file) - whence;
      fseek(file, SEEK_END, -1*(whence));
    }
    printf("%c", c);

  }
  fclose(file);
  return 0;

}
