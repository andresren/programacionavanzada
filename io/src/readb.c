
#include <stdio.h>
#include <stdlib.h>
unsigned debug;
#define printdebug(msg) if (debug) printf("%s\n",(msg))
#define SIZE 10

int main(){
  debug = 0;
  char *value;
  char message[100];
  char array[SIZE] = {1}, i;



  FILE *file;
  int num;
  long whence;
  value = getenv("DEBUG");
  if(value && strcmp(value,"1") == 0)
    debug = 1;

  file = fopen("output.bin","wb");

  if(file == NULL){
    sprintf(message, "Error opening file ...");
    printdebug(message);
    return -1;
  }
  fread(array, SIZE, sizeof(int), file);

  fclose(file);
  for(i =0;i<SIZE;i++){
    printf("%d\n",array[i]);
  }
  return 0;

}
