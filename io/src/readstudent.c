
#include <stdio.h>
#include <stdlib.h>
unsigned debug;
#define printdebug(msg) if (debug) printf("%s\n",(msg))
#define SIZE 10
typedef struct{
  int matriucla;
  char semestre[10];
  int calificaciones[SIZE];
} Student;

int main(int argc, char*argv[]){

  debug = 0;
  char *value;
  char message[100];
  Student estudiante;
  int pos;
  int i;

  FILE *file;
  int num;
  long whence;
  value = getenv("DEBUG");
  if(value && strcmp(value,"1") == 0)
    debug = 1;

  file = fopen("student.db","wb");

  if(file == NULL){
    sprintf(message, "Error opening file ...");
    printdebug(message);
    return -1;
  }
  pos = atoi(argv[1]);
  whence = pos*sizeof(Student);
  fseek(file,SEEL_SET,whence);
  fread(&estudiante, 1, sizeof(Student), file);
  fclose(file);
  printf("%s\n", "Estudiante : " );
  printf("Matricula %d\n", estudiante.matriucla);
  printf("Semestre %s",estudiante.semestre );

  for(i =0;i<SIZE;i++){
    printf("CAL[%d]\n",i+1,estudiante.calificaciones[i]);
  }

  return 0;

}
