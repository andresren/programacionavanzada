#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>

#define BUFSIZE 10
#define MAXINT 20
unsigned int buf[BUFSIZE];

long unsigned account;
sem_t mutex;
sem_t available;
sem_t ready;
sem_t cmutex;

#define NUM_CONSUMER 4

#define NUM_PRODUCER 4
#define NUM_THREADS (NUM_PRODUCER+NUM_CONSUMER)
unsigned int ptotal;
unsigned int ppos;
sem_t ptotal_mutex;
sem_t ppos_mutex;

void * producer(void *id){
  unsigned int pos = 0;
  unsigned int value = 0;
  printf("I am the producer \n");
  while(1){

    sem_wait(&ptotal_mutex);
    if(ptotal++ >= MAXINT){

      sem_post(&ptotal_mutex);
      break;
    }

    value = MAXINT-ptotal+1;
    sem_post(&ptotal_mutex);

    sem_wait(&available);

    sem_wait(&ppos_mutex);
    pos = ppos;
    ppos = (++ppos)%BUFSIZE;
    sem_post(&ppos_mutex);

    buf[pos] = value;
    sem_post(&ready);

  }
  printf("done producing \n" );
}

unsigned int cpos;
unsigned int ctotal;
sem_t total_cmutex;
void * consumer(void *id){

  unsigned int numelementosconsume = MAXINT;
  unsigned int pos;
  printf("I am the consumer \n");
  while(1){


    sem_wait(&total_cmutex);
    if(ctotal++ >= MAXINT){

      break;
    }
    sem_post(&total_cmutex);

    sem_wait(&ready);
    sem_wait(&cmutex);
    pos = cpos;
    cpos = (++cpos)%BUFSIZE;
    sem_post(&cmutex);


    printf("value is %u \n",buf[pos]);
    sem_post(&available);

    numelementosconsume -=1;




  }
  sem_post(&total_cmutex);

  printf("done consuming \n" );
  return NULL;
}

int main(){
  pthread_t threads[NUM_THREADS];
  long unsigned i;
  sem_init(&available,0,BUFSIZE);
  sem_init(&ready,0,0);
  sem_init(&cmutex,0,1);


  account=0;
  ctotal = 0;
  sem_init(&total_cmutex,0,1);
  ppos=0;
  sem_init(&ppos_mutex,0,1);
  sem_init(&ptotal_mutex,0,1);

  pthread_create(&threads[1], NULL, producer, NULL);
  for(i = 0; i<NUM_PRODUCER; i++){
    pthread_create(&threads[i], NULL, producer, NULL);
  }

  for(; i<NUM_THREADS; i++){
    pthread_create(&threads[i], NULL, consumer, NULL);
  }

  for(i = 0; i<NUM_THREADS; i++){
    pthread_join(threads[i], NULL);

  }
  printf("total %lu\n", account);


  return 0;

}
