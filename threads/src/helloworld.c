#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>

long unsigned account;
sem_t mutex;

void * add(void *id){
  printf("hilo\n" );
  sem_wait(&mutex);
  account += 100;
  sem_post(&mutex);
  //printf("Hello world, i am %ld\n", (long unsigned)id);
  printf("hilo\n" );
  return NULL;
}


#define NUM_THREADS 20000

int main(){
  pthread_t threads[NUM_THREADS];
  long unsigned i;
  sem_init(&mutex,0,1);
  account=0;
  for(i = 0; i<NUM_THREADS; i++){
    pthread_create(&threads[i], NULL, add, NULL);

  }


  for(i = 0; i<NUM_THREADS; i++){
    pthread_join(threads[i], NULL);

  }
  printf("total %lu\n", account);


  return 0;

}
