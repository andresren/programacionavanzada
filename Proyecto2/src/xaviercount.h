//Nombre:xaviercount header
//andres Renteria
//16/10/2016

#ifndef __XAVIERCOUNTH__
#define __XAVIERCOUNTH__

void calculate(int *numbers, int *a, int m, int p, int offset);

int getLast(int *a, int m, int p);

int getOffset(int *a, int p);

void printOutput(Vector result);

#endif
