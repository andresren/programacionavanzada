//Nombre:xaviercount
//andres Renteria
//16/10/2016

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include "vector.h"

void calculate(int *numbers, int *a, int m, int p, int offset){
  int j,k,h,c,d;
  for(j = 0; j <= m - p;j++){
    if(p > 1){
      for(k = j+1;k <= m - p + 1;k++){
        if(p > 2){
          for(h = k+1 ;h <= m - p + 2;h++){
            if(p>3){
              for(c = h+1;c <= m-p + 3; c++){
                if(p>4){
                  for(d = c+1; d <= m-p+4;d++){
                    numbers[a[j]+ a[k]+ a[h] + a[c] + a[d] - offset] += 1;
                    //printf("%d \n", a[j] + a[k] + a[h] + a[c] + a[d]);
                  }
                }
                else{
                  numbers[a[j]+ a[k]+ a[h] + a[c] - offset] += 1;
                  //printf("%d\n", a[j] + a[k] + a[h] + a[c]);
                }
              }
            }
            else{
              numbers[a[j]+ a[k]+ a[h]-offset] += 1;
              //printf("%d\n", a[j] + a[k] + a[h]);
            }
          }
        }
        else{
          numbers[a[j]+ a[k]-offset] += 1;
          //printf("%d\n", a[j] + a[k]);
        }
      }
    }
    else{
      numbers[a[j]-offset] += 1;
      //printf("%d\n", a[j]);
    }

  }
}

int getLast(int *a, int m, int p){
  int j, last;
  for(j = m-1; j > m-p-1; j--){
    last = last + a[j];

  }
  return last;
}

int getOffset(int *a, int p){
  int j,offset;
  for(j = 0; j < p; j++){
    offset = offset + a[j];
  }
  return offset;
}

void printOutput(Vector result){
  int k,j;
  for(j = 0;j < VectorLength(&result)-1;j++){
    VectorNth(&result, &k, j);

    if(k>=0){
      VectorNth(&result, &k, j+1);
      if (k>0){
        VectorNth(&result, &k, j);
        printf("%d: ", k);
        VectorNth(&result, &k, j+1);
        printf("%d\n", k);
      }

      j++;
    }else{
      j++;
      VectorNth(&result, &k, j);
      printf("\n");
      printf("Case #%d\n", k+1);

    }
  }
}


int main(){
    int n, m, p, x, offset, last;
    int i,j,k,l,h,c,d,tmp;
    Vector result;
    int elemSize = sizeof(int);
    int initialAllocation = 1;
    VectorNew(&result,elemSize,initialAllocation);

    scanf("%d",&n);
    for(i = 0; i < n; i++){
      tmp = -1;
      VectorAppend(&result, &tmp);
      VectorAppend(&result,&i);
      scanf("%d %d",&m,&p);
      int *a = malloc(sizeof(int) * m);
      for(j = 0; j < m; j++){
        scanf("%d",&a[j]);

      }
      offset = getOffset(a,p);
      last = getLast(a,m,p);
      int *numbers = malloc(sizeof(int) * last-offset);



      calculate(numbers, a, m, p, offset);


      for(j = 0; j<=last-offset;j++){
        tmp =j+offset;
        VectorAppend(&result,&tmp);
        VectorAppend(&result,&numbers[j]);

      }
    }

    printOutput(result);

    return 0;
}
