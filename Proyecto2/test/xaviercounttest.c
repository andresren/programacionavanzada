//Nombre:xaviercount test
//andres Renteria
//16/10/2016

#include <stdio.h>
#include "utest.h"
#include "vector.h"
#include "../src/xaviercount.h"
#include "string.h"
#include <stdlib.h>
#include <assert.h>

int offset, last;
int *a;
int m, p;
int *n;
static char * test_get_offset(){
  m = 2;
  p = 2;
  int i;
  a = malloc(sizeof(int) * m);
  for(i=1; i<=m;i++){
    a[i-1]=i;
  }
  offset = getOffset(a, p);
  assert_test("the offset must be 3", offset == 3);
  return 0;

}

static char * test_get_last(){
  last = getLast(a,m,p);
  assert_test("the last must be 3", last == 3);
  return 0;
}

static char * test_calculate(){

  int tmp = -1;
  int cases = 0;
  /*VectorAppend(&n, &tmp);
  VectorAppend(&n, &cases);*/
  n = malloc(sizeof(int) * last-offset);
  calculate(n, a, m, p, offset);
  assert_test("position 3 - the offset must have 1 ", n[3-offset]==1);
  return 0;
}

static char * test_print_output(){
  Vector result;
  int elemSize = sizeof(int);
  int initialAllocation = 1;
  VectorNew(&result,elemSize,initialAllocation);
  int tmp = -1;
  int cases = 0;
  VectorAppend(&result, &tmp);
  VectorAppend(&result, &cases);
  tmp = 3;
  VectorAppend(&result, &tmp);
  VectorAppend(&result, &n[3-offset]);
  printOutput(result);
  return 0;
}

int main(){
  run_test("Test get offset:",test_get_offset);
  run_test("Test get last:",test_get_last);
  run_test("Test calculate:",test_calculate);
  run_test("Test print output:",test_print_output);
  return 0;

}
